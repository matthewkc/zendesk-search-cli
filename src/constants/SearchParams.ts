export enum ESearchTypes {
  Users = 'Users',
  Tickets = 'Tickets',
  Organizations = 'Organizations',
}

export interface ISearchParams {
  searchType: ESearchTypes;
  searchTerm: string;
  searchValue: string;
}
