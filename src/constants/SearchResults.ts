export interface IUser {
  _id: number;
  url: string;
  external_id: string;
  name: string;
  alias: string;
  created_at: string;
  active: boolean;
  verified: boolean;
  shared: boolean;
  locale: string;
  timezone: string;
  last_login_at: string;
  email: string;
  phone: string;
  signature: string;
  organization_id: number;
  tags: string[];
  suspended: boolean;
  role: string;
}

export interface ITicket {
  _id: string;
  url: string;
  external_id: string;
  created_at: string;
  type: string;
  subject: string;
  description: string;
  priority: string;
  status: string;
  submitter_id: number;
  assignee_id: number;
  organization_id: number;
  tags: string[];
  has_incidents: boolean;
  due_at: string;
  via: string;
}

export interface IOrganization {
  _id: number;
  url: string;
  external_id: string;
  name: string;
  domain_names: string[];
  created_at: string;
  details: string;
  shared_tickets: boolean;
  tags: string[];
}

export interface IUserSearchResults {
  matchedItem: IUser;
  organisation: IOrganization[];
  submittedTicket: ITicket[];
  assignedTicket: ITicket[];
}

export interface ITicketSearchResults {
  matchedItem: ITicket;
  organisation: IOrganization[];
  submittedUser: IUser[];
  assignedUser: IUser[];
}

export interface IOrganizationSearchResults {
  matchedItem: IOrganization;
  userList: IUser[];
  ticketList: ITicket[];
}

export type TSearchResult = IUserSearchResults | ITicketSearchResults | IOrganizationSearchResults;
