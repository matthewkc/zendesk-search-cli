export const WelcomeMessage = 'Welcome to Zendesk Search.';

export const EntryQuestions = [
  {
    type: 'input',
    name: 'entryQuestion',
    message:
      "Type 'quit' to exit at any time, Press 'Enter' to continue \n" +
      '\n' +
      '\tSelect search options:\n' +
      '\t* Press 1 to search Zendesk\n' +
      '\t* Press 2 to view a list of searchable fields\n' +
      "\t* Type 'quit' to exit\n",
    validate: (value: string) => {
      if (['1', '2', 'quit'].includes(value.toLowerCase())) {
        return true;
      }

      return "Please enter a valid option or Type 'quit' to exit\n";
    },
  },
];

export const SearchZendeskQuestions = [
  {
    type: 'input',
    name: 'searchQuestion',
    message: 'Search 1) Users or 2) Tickets or 3) Organizations\n',
    validate: (value: string) => {
      if (['1', '2', '3'].includes(value.toLowerCase())) {
        return true;
      }
      return 'Please enter a valid option';
    },
  },
  {
    type: 'input',
    name: 'searchTerm',
    message: 'Enter search term\n',
  },
  {
    type: 'input',
    name: 'searchValue',
    message: 'Enter search value\n',
  },
];
