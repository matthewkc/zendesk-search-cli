import * as fs from 'fs';
import * as path from 'path';
import { chain } from 'stream-chain';
import { parser } from 'stream-json';
import { ISearchParams } from '../constants/searchParams';
import { TSearchResult } from '../constants/SearchResults';

export interface IJSONHelper {
  createStream(searchParams: ISearchParams): Promise<TSearchResult[]>;
}

export default class JSONHelper {
  public async createStream(searchParams: ISearchParams): Promise<TSearchResult[]> {
    return [];
  }
}
