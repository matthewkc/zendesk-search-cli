import * as chalk from 'chalk';

export default {
  show(status: string, message: string) {
    switch (status) {
      case 'error':
        console.log(chalk.red(message));
        break;
      case 'success':
        console.log(chalk.green(message));
        break;
      case 'info':
        console.log(chalk.cyan(message));
        break;
      default:
        console.log(message);
        break;
    }
  },
  print(message: string) {
    this.show('info', message);
  },
  error(message: string) {
    this.show('error', 'Error: '.concat(message));
  },
  success(message: string) {
    this.show('success', 'Success: '.concat(message));
  },
};
