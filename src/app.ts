import Welcome from './components/welcome';
import SearchOptions from './components/entryQuestion';

export async function App(): Promise<any> {
  const welcomeBanner = new Welcome();
  welcomeBanner.printMessage();

  const searchOptions = new SearchOptions();
  while ((await searchOptions.printOptions()) !== 'quit') {}
}
