import * as inquirer from 'inquirer';
import { SearchZendeskQuestions } from '../../constants/copy';
import { ESearchTypeMapping } from '../../constants/SearchTypeMapping';
import { ESearchTypes, ISearchParams } from '../../constants/SearchParams';

export interface ISearchZendesk {
  printSearchZendeskOptions(): Promise<void>;
}

export default class SearchZendesk implements ISearchZendesk {
  public async printSearchZendeskOptions(): Promise<void> {
    const searchZendeskAnswer = await inquirer.prompt(SearchZendeskQuestions);

    const searchType = ESearchTypeMapping[searchZendeskAnswer.searchTerm];
    const searchParams: ISearchParams = {
      searchType: ESearchTypes[searchType as keyof typeof ESearchTypes],
      searchTerm: searchZendeskAnswer.searchTerm.trim(),
      searchValue: searchZendeskAnswer.searchTerm.trim(),
    };
  }
}
