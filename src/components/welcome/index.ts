import { WelcomeMessage } from '../../constants/copy';
import Logger from '../../helpers/LoggerHelper';

export interface IWelcome {
  printMessage(): void;
}

export default class Welcome implements IWelcome {
  public printMessage() {
    Logger.print(WelcomeMessage);
  }
}
