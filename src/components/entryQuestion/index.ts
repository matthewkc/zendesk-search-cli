import * as inquirer from 'inquirer';
import { EntryQuestions } from '../../constants/copy';
import SearchZendesk from '../searchZendesk';

export interface IEntryQuestion {
  printOptions(): Promise<string | null>;
}

export default class EntryQuestion implements IEntryQuestion {
  public async printOptions(): Promise<string | null> {
    const questions = await inquirer.prompt(EntryQuestions);
    switch (questions.entryQuestion.toLowerCase()) {
      case '1':
        const searchZendesk = new SearchZendesk();
        await searchZendesk.printSearchZendeskOptions();
        return null;
      case '2':
        return null;
      case 'quit':
        return 'quit';
      default:
        return null;
    }
  }
}
